import { Routes, Route } from "react-router-dom";

import PostDetails from "./components/post/details";
import PostUser from "./components/post/user";
import PostAll from "./components/post/all";
import MainNavigation from "./components/main-navigation";
import PageNotFound from "./components/page-not-found";

const App = () => {
  return (
    <div className="app">
      <Routes>
        <Route path="/" element={<MainNavigation />}>
          <Route index element={<PostAll />} />
          <Route path="posts/:postId" element={<PostDetails />} />
          <Route path="users/:userId" element={<PostUser />} />
          <Route path="*" element={<PageNotFound />} />
        </Route>
      </Routes>
    </div>
  );
};

export default App;
