import { useEffect } from "react";
import { Link, Outlet, useLocation } from "react-router-dom";

const MainNavigation = () => {
  const { pathname } = useLocation();
  useEffect(() => {
    document.documentElement.scrollTo({ top: 0, left: 0, behavior: "auto" });
  }, [pathname]);

  return (
    <div>
      <nav>
        <Link to="/">Home</Link>
      </nav>
      <Outlet />
    </div>
  );
};

export default MainNavigation;
