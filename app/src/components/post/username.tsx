import type { FC } from "react";
import useAxios from "axios-hooks";

type ApiResponse = { name: string };

const Username: FC<{ userId: number }> = ({ userId }) => {
  const [{ data, loading, error }] = useAxios<ApiResponse>(`https://jsonplaceholder.typicode.com/users/${userId}`);

  // provide a graceful fallbacks, as we always want to provide something for the user to see and click on
  if (loading) return <span>&hellip;</span>;
  if (error) return <span>{userId}</span>;

  return <span>{data?.name ?? userId}</span>;
};

export default Username;
