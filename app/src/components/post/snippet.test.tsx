import { render } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import PostSnippet from "./snippet";

describe("Post - Snippet", () => {
  test("render", async () => {
    const { container } = render(
      <MemoryRouter>
        <PostSnippet id={1} title="My first post" body="quas totam nostrum rerum" />
      </MemoryRouter>
    );
    expect(container).toMatchSnapshot();
  });
});
