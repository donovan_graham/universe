import type { FC } from "react";
import { Link } from "react-router-dom";

const PostSnippet: FC<{ id: number; title: string; body: string }> = ({ id, title, body }) => {
  return (
    <div style={{ marginBottom: "3rem" }}>
      <h3
        style={{
          display: "-webkit-box",
          WebkitLineClamp: 1,
          WebkitBoxOrient: "vertical",
          overflow: "hidden",
        }}
      >
        {title}
      </h3>
      <div
        style={{
          display: "-webkit-box",
          WebkitLineClamp: 2,
          WebkitBoxOrient: "vertical",
          overflow: "hidden",
        }}
      >
        {body}
      </div>
      <Link to={`/posts/${id}`}>read more</Link>
    </div>
  );
};

export default PostSnippet;
