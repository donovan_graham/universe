import { render } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import useAxios from "axios-hooks";

import Username from "./username";

jest.mock("axios-hooks");

const userFixture = {
  id: 1,
  name: "Leanne Graham",
  username: "Bret",
  email: "Sincere@april.biz",
  address: {
    street: "Kulas Light",
    suite: "Apt. 556",
    city: "Gwenborough",
    zipcode: "92998-3874",
    geo: {
      lat: "-37.3159",
      lng: "81.1496",
    },
  },
  phone: "1-770-736-8031 x56442",
  website: "hildegard.org",
  company: {
    name: "Romaguera-Crona",
    catchPhrase: "Multi-layered client-server neural-net",
    bs: "harness real-time e-markets",
  },
};

describe("Post - Username", () => {
  test("render", async () => {
    (useAxios as jest.MockedFunction<typeof useAxios>).mockReturnValue([
      {
        data: userFixture,
        error: null,
        loading: false,
      },
      jest.fn(),
      jest.fn(),
    ]);

    const { container } = render(
      <MemoryRouter>
        <Username userId={1} />
      </MemoryRouter>
    );
    expect(container).toMatchSnapshot();
  });
});
