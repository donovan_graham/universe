import { Fragment } from "react";
import { useParams } from "react-router-dom";
import useAxios from "axios-hooks";

import Username from "./username";
import PostSnippet from "./snippet";

type ApiResponse = { id: number; title: string; body: string }[];

const PostUser = () => {
  const { userId } = useParams<{ userId: string }>();
  const [{ data, loading, error }, refetch] = useAxios<ApiResponse>(`https://jsonplaceholder.typicode.com/users/${userId}/posts`);

  if (loading) return <p>Loading...</p>;
  if (error)
    return (
      <p>
        Error! <a onClick={() => refetch()}>retry</a>
      </p>
    );

  return (
    <Fragment>
      <h1>
        <Username userId={Number(userId)} />
        's posts
      </h1>
      <button onClick={() => refetch()}>refresh</button>

      {data?.length === 0 && <div>This user doesn't have any post yet, please check later.</div>}

      {data?.map(({ id, title, body }) => (
        <PostSnippet key={id} id={id} title={title} body={body} />
      ))}
    </Fragment>
  );
};

export default PostUser;
