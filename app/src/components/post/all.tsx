import { Fragment } from "react";
import useAxios from "axios-hooks";

import PostSnippet from "./snippet";

type ApiResponse = { id: number; title: string; body: string }[];

function PostAll() {
  const [{ data, loading, error }, refetch] = useAxios<ApiResponse>("https://jsonplaceholder.typicode.com/posts");

  if (loading) return <p>Loading...</p>;
  if (error)
    return (
      <p>
        Error! <a onClick={() => refetch()}>retry</a>
      </p>
    );

  return (
    <Fragment>
      <h1>All posts</h1>
      <button onClick={() => refetch()}>refesh</button>

      {data?.length === 0 && <div>No are no posts, please check later.</div>}

      {data?.map(({ id, title, body }) => (
        <PostSnippet key={id} id={id} title={title} body={body} />
      ))}
    </Fragment>
  );
}

export default PostAll;
