import type { FC } from "react";
import { Fragment } from "react";
import useAxios from "axios-hooks";

type ApiResponse = { id: number; name: string; body: string }[];

const PostComments: FC<{ postId: number }> = ({ postId }) => {
  const [{ data, loading, error }, refetch] = useAxios<ApiResponse>(`https://jsonplaceholder.typicode.com/posts/${postId}/comments`);

  if (loading) return <p>Loading...</p>;
  if (error) {
    return (
      <p>
        Error! <a onClick={() => refetch()}>retry</a>
      </p>
    );
  }

  return (
    <Fragment>
      <h2>Comments</h2>

      {data?.length === 0 && <div>No comments found for this post</div>}

      {data?.map(({ id, name, body }) => (
        <div key={id} style={{ marginBottom: "2rem" }}>
          <div style={{ fontSize: 14, fontStyle: "italic" }}>{name}</div>
          <div>{body}</div>
        </div>
      ))}
    </Fragment>
  );
};

export default PostComments;
