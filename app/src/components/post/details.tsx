import { Link, useParams } from "react-router-dom";
import useAxios from "axios-hooks";

import Username from "./username";
import PostComments from "./comments";

type ApiResponse = { userId: number; id: number; title: string; body: string };

const PostDetails = () => {
  const { postId } = useParams<{ postId: string }>();

  const [{ data, loading, error }, refetch] = useAxios<ApiResponse>(`https://jsonplaceholder.typicode.com/posts/${postId}`);

  if (loading) return <p>Loading...</p>;
  if (error)
    return (
      <p>
        Error! <a onClick={() => refetch()}>retry</a>
      </p>
    );

  const { userId, id, title, body } = data!;

  return (
    <div data-testid="post-details">
      <h1>{title}</h1>
      <h4>
        more posts by{" "}
        <Link to={`/users/${userId}`}>
          <Username userId={userId} />
        </Link>
      </h4>
      <p>{body}</p>

      <br />
      <PostComments postId={id} />
    </div>
  );
};

export default PostDetails;
