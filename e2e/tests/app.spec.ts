import fs from "fs";
import path from "path";
import { test, expect } from "@playwright/test";

const distMountPath = "../../app/dist";
const appBaseUrl = "https://testapp/";
const apiBaseUrl = "https://jsonplaceholder.typicode.com/";

test.use({ viewport: { width: 1280, height: 1024 } });

test.beforeEach(async ({ page }, testInfo) => {
  await page.route("**/*", async (route, request) => {
    const url = request.url();

    // index.html
    if (request.isNavigationRequest()) {
      const filePath = path.resolve(__dirname, distMountPath, "index.html");
      const fileExists = await fs.existsSync(filePath);
      if (fileExists) {
        route.fulfill({ path: filePath });
        return;
      }
    }

    // assets
    if (url.startsWith(appBaseUrl)) {
      const filePath = path.resolve(__dirname, distMountPath, url.replace(appBaseUrl, ""));
      const fileExists = await fs.existsSync(filePath);
      if (fileExists) {
        route.fulfill({ path: filePath });
        return;
      }
    }

    // api response
    if (url.startsWith(apiBaseUrl)) {
      const filePath = path.resolve(__dirname, `${url.replace(apiBaseUrl, "__fixtures__/")}.json`);
      const fileExists = await fs.existsSync(filePath);
      if (fileExists) {
        route.fulfill({ path: filePath });
        return;
      }
    }

    console.error("ERROR: no interception configured for >>>", request.method(), request.url());
    route.abort();
    return;
  });
});

test("homepage", async ({ page }) => {
  await page.goto(appBaseUrl);
  await page.waitForLoadState("networkidle");
  await expect(page).toHaveTitle("Universe App");
  await expect(page).toHaveScreenshot();
});

test("posts/1", async ({ page }) => {
  await page.goto(`${appBaseUrl}posts/1`);
  await page.waitForLoadState("networkidle");
  await expect(page).toHaveScreenshot();
});

test("users/1", async ({ page }) => {
  await page.goto(`${appBaseUrl}users/1`);
  await page.waitForLoadState("networkidle");
  await expect(page).toHaveScreenshot();
});
