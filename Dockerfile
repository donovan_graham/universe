# https://hub.docker.com/_/node
FROM node:16-alpine

RUN apk add --update make gcc g++ git python3 curl ca-certificates wget bash gcompat openssh

WORKDIR /srv
