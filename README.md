# General

## Docker
I like to have a consistent base image to use across different development and continuous integration (CI) machines. It also keeps the host OS clean of application specific dependencies, node versions, binaries, etc.  This helps with onboarding and cold start setups. Any container technology could be used in place of Docker, but I'm pretty familiar with [Docker](https://www.docker.com/) and [Kubernetes](https://kubernetes.io/), so lets start with it.

## Vite
I've worked with [Webpack](https://webpack.js.org/) forever, and wanted to give another project a try. This is my first experience with [Vite](https://vitejs.dev/guide/why.html). It uses Rollup and esbuild and has HMR pre-configured. Lets see how it works out :)

## Styling
I was going to use [styled-component](https://styled-components.com/), but ended up having a very minimal design and felt this was over-kill. Ended up using a simple index.css global style for centering and laying out content, and React [inline style](https://reactjs.org/docs/dom-elements.html#style) objects for spacing. No need to over-complicate things at this stage.

## Testing
Unit tests run on [Jest](https://jestjs.io/) and [testing-library](https://testing-library.com/).

E2E tests run on [Playwright](https://playwright.dev) using headless browsers with image regression testing.  See `e2e/tests/__snapshots__/app.spec.ts-snapshots` folder for examples of snapshots.

## Code style
Use [prettier](https://prettier.io/) to format files in consistent manner. 

TODO: Setup and configure [eslint](https://eslint.org/).

## Continuous Integration (CI)
Using [Gitlab CI](https://docs.gitlab.com/ee/ci/) and a build and deploy pipeline.  I like their integrated tooling and devops offering.

## Continuous Deployment (CD)
TODO: Deploy to AWS or Google Cloud or Cloudflare depending on the requirement.



# Running App locally

Install application dependencies using docker
```
docker-compose run --rm app npm install
```

Run the app
```
docker-compose up app
```

Navigate to url in browser
```
http://http://localhost:5173/
```



# Running E2E/Integration test

NOTE: Always use docker to run all E2E tests, as this ensures that all the browser versions, capababilities and local fonts used are consistent across local and ci development.

Use the [base docker image](https://playwright.dev/docs/docker) which is provided by playwright. 

Install dependencies
```
docker-compose run --rm e2e npm install
```

Make sure you have a recent production build from source
```
docker-compose run --rm app npm run build
```

Run the tests
```
docker-compose run --rm e2e npm run test
```


## Debugging E2E setup

References:
https://cntnr.io/running-guis-with-docker-on-mac-os-x-a14df6a76efc
https://kartoza.com/en/blog/how-to-run-a-linux-gui-application-on-osx-using-docker/


### On Mac
Install [xquartz](https://www.xquartz.org/)

Install socat [using brew](https://formulae.brew.sh/formula/socat) or [using macports](https://ports.macports.org/port/socat/)
 
Configure docker-compose service 
```
    environment: 
    - DISPLAY:host.docker.internal:0
```
   
https://docs.docker.com/desktop/mac/networking/#i-want-to-connect-from-a-container-to-a-service-on-the-host
We recommend that you connect to the special DNS name `host.docker.internal` which resolves to the internal IP address used by the host. This is for development purpose and will not work in a production environment outside of Docker Desktop for Mac.

Run Socat in seperate tab when debugging tests
```
socat TCP-LISTEN:6000,reuseaddr,fork UNIX-CLIENT:\"$DISPLAY\"
```


Run the tests in debug mode
```
docker-compose run --rm e2e npm run test:debug
```
